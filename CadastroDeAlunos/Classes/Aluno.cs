﻿using System;

namespace CadastroDeAlunos.Classes
{
    public class Aluno
    {
        public int Codigo { get; set; }
        public String nome { get; set; }
        public Int32 Idade { get; set; }
        public String Endereco { get; set; }
        public String Telefone { get; set; }
        public String Email { get; set; }
        public String Cidade { get; set; }
        public String Uf { get; set; }
        public String NomePAi { get; set; }
        public String NomeMae { get; set; }

    }
}
