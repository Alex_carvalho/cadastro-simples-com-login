﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadastroDeAlunos.Classes;

namespace CadastroDeAlunos.Infra
{
    class SisDBA
    {
        private string srtCoon = @"Data Source=ALEXANDRE-VAIO\alex;Initial Catalog=CadastroAlunos;Persist Security Info=True;User ID=sa;Password=12345";
        private string _Sql = "";
        private SqlConnection sqlConn = null; 

        private bool Conectar()
        {
            sqlConn = new SqlConnection(srtCoon);
            try
            {
                sqlConn.Open();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool Desconectar()
        {
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
                sqlConn.Dispose();
                return true;
            }
            else
            {
                sqlConn.Dispose();
                return false;
            }
        }

        public bool Insert(Aluno aluno)
        {
            _Sql = "INSERT INTO [dbo].[Alunos]([Nome],[Idade],[Endereco],[Telefone],[Email],[Cidade],[UF],[NomePai],[NomeMae]) " +
                   "VALUES(@Nome, @Idade, @Endereco, @Telefone, @Email, @Cidade, @UF, @NomePai, @NomeMae);";

            SqlCommand objCmd = null;

            if (this.Conectar())
            {
                try
                {
                    objCmd = new SqlCommand(_Sql, sqlConn);
                    objCmd.Parameters.Add(new SqlParameter("@nome", aluno.nome));
                    objCmd.Parameters.Add(new SqlParameter("@Idade", aluno.Idade));
                    objCmd.Parameters.Add(new SqlParameter("@Endereco", aluno.Endereco));
                    objCmd.Parameters.Add(new SqlParameter("@Telefone", aluno.Telefone));
                    objCmd.Parameters.Add(new SqlParameter("@Email", aluno.Email));
                    objCmd.Parameters.Add(new SqlParameter("@Cidade", aluno.Cidade));
                    objCmd.Parameters.Add(new SqlParameter("@UF", aluno.Uf));
                    objCmd.Parameters.Add(new SqlParameter("@NomePai", aluno.NomePAi));
                    objCmd.Parameters.Add(new SqlParameter("@NomeMae", aluno.NomeMae));

                    objCmd.ExecuteNonQuery();

                    return true;
                }
                catch (Exception sqlerr)
                {
                    throw sqlerr;
                }
                finally
                {
                    this.Desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int id_aluno)
        {
            _Sql =
                "delete Alunos where Id_Aluno = @Id_Aluno";

            SqlCommand objcmd = null;

            if (this.Conectar())
            {
                try
                {
                    objcmd = new SqlCommand(_Sql, sqlConn);
                    objcmd.Parameters.AddWithValue("@Id_Aluno", id_aluno);
                    objcmd.ExecuteNonQuery();

                    objcmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception sqlerr)
                {
                    throw sqlerr;
                }
                finally
                {
                    this.Desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public bool UpDate(Aluno aluno)
        {
            _Sql =
                "UPDATE [dbo].[Alunos] SET [Nome] = @Nome,[Idade] = @Idade,[Endereco] = @Endereco,[Telefone] = @Telefone,[Email] = @Email,[Cidade] = @Cidade,[UF] = @UF,[NomePai] = @NomePai,[NomeMae] = @NomeMae WHERE Id_Aluno = @Id_Aluno;";


            SqlCommand objCmd = null;

            if (this.Conectar())
            {
                try
                {
                    objCmd = new SqlCommand(_Sql, sqlConn);
                    objCmd.Parameters.Add(new SqlParameter("@nome", aluno.nome));
                    objCmd.Parameters.Add(new SqlParameter("@Idade", aluno.Idade));
                    objCmd.Parameters.Add(new SqlParameter("@Endereco", aluno.Endereco));
                    objCmd.Parameters.Add(new SqlParameter("@Telefone", aluno.Telefone));
                    objCmd.Parameters.Add(new SqlParameter("@Email", aluno.Email));
                    objCmd.Parameters.Add(new SqlParameter("@Cidade", aluno.Cidade));
                    objCmd.Parameters.Add(new SqlParameter("@UF", aluno.Uf));
                    objCmd.Parameters.Add(new SqlParameter("@NomePai", aluno.NomePAi));
                    objCmd.Parameters.Add(new SqlParameter("@NomeMae", aluno.NomeMae));
                    objCmd.Parameters.Add(new SqlParameter("@Id_Aluno", aluno.Codigo));

                    objCmd.ExecuteNonQuery();

                    return true;
                }
                catch (Exception sqlerr)
                {
                    throw sqlerr;
                }
                finally
                {
                    this.Desconectar();
                }
            }
            else
            {
                return false;
            }
        }

        public DataTable ListaGrid()
        {
            _Sql =
                "Select [Id_Aluno]as Código, [Nome],[Idade],[Endereco],[Telefone],[Email],[Cidade] From Alunos";

            SqlCommand objcmd = null;

            if (this.Conectar())
            {
                try
                {
                    objcmd = new SqlCommand(_Sql, sqlConn);
                    var adp = new SqlDataAdapter(objcmd);
                    var dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }
                catch (Exception sqlerr)
                {
                    throw sqlerr;
                }
                finally
                {
                    this.Desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public DataTable Pesquisar(string sql, string param)
        {
            this._Sql = sql;

            SqlCommand objcmd = null;

            if (this.Conectar())
            {
                try
                {
                    objcmd = new SqlCommand(_Sql, sqlConn);
                    objcmd.Parameters.Add(new SqlParameter("@valor", param));
                    var adp = new SqlDataAdapter(objcmd);
                    var dt = new DataTable();
                    adp.Fill(dt);

                    return dt;
                }
                catch (Exception sqlerr)
                {
                    throw sqlerr;
                }
                finally
                {
                    this.Desconectar();
                }
            }
            else
            {
                return null;
            }
        }

        public List<string> ListaUf()
        {
            _Sql = "SELECT UF FROM ESTADO";

            SqlCommand objCmd = null;
            
            var uf = new List<string>();

            if (this.Conectar())
            {
                try
                {
                    objCmd = new SqlCommand(_Sql, sqlConn);

                    SqlDataReader dr = objCmd.ExecuteReader();

                    while (dr.Read())
                    {
                        uf.Add(dr["UF"].ToString());
                    }

                    return uf;
                }
                catch (Exception sqlerr)
                {
                    throw sqlerr;
                }
                finally
                {
                    this.Desconectar();
                }
            }
            else
            {
                return null;
            }
        } 

        public List<string> ListaCidade(string uf)
        {
            _Sql = "SELECT NOME FROM CIDADE WHERE UF = @UF";

            SqlCommand objCmd = null;
            
            var cidade = new List<string>();

            if (this.Conectar())
            {
                try
                {
                    objCmd = new SqlCommand(_Sql, sqlConn);
                    
                    objCmd.Parameters.AddWithValue("@UF", uf);
                    
                    SqlDataReader dr = objCmd.ExecuteReader();

                    while (dr.Read())
                    {
                        cidade.Add(dr["Nome"].ToString());
                    }

                    return cidade;
                }
                catch (Exception sqlerr)
                {
                    throw sqlerr;
                }
                finally
                {
                    this.Desconectar();
                }
            }
            else
            {
                return null;
            }
        } 
    }
}
