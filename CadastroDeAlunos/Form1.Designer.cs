﻿namespace CadastroDeAlunos
{
    partial class FrmCrud
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCrud));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageAdd = new System.Windows.Forms.TabPage();
            this.cboCidade = new System.Windows.Forms.ComboBox();
            this.cboUfAdd = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMAeAdd = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTeladd = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtIdadeAdd = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPaiAdd = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtEmailAdd = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtEndAdd = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNomeAdd = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dgAdcionar = new System.Windows.Forms.DataGridView();
            this.tabPageEditar = new System.Windows.Forms.TabPage();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.cbCidade = new System.Windows.Forms.ComboBox();
            this.cbUF = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMae = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtIdade = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPai = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.dtEditar = new System.Windows.Forms.DataGridView();
            this.tabPageExcluir = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtExcluir = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtExcluir = new System.Windows.Forms.DataGridView();
            this.tabPagePesq = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rbNome = new System.Windows.Forms.RadioButton();
            this.rbCodigo = new System.Windows.Forms.RadioButton();
            this.buttonPesquisar = new System.Windows.Forms.Button();
            this.txtPesquisa = new System.Windows.Forms.TextBox();
            this.dtPesquisar = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblDateTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnCad_Aluno = new System.Windows.Forms.ToolStripButton();
            this.btnAtualiza_Aluno = new System.Windows.Forms.ToolStripButton();
            this.btnDeletar = new System.Windows.Forms.ToolStripButton();
            this.btnSair = new System.Windows.Forms.ToolStripButton();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdcionar)).BeginInit();
            this.tabPageEditar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEditar)).BeginInit();
            this.tabPageExcluir.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtExcluir)).BeginInit();
            this.tabPagePesq.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPesquisar)).BeginInit();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tabControl1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 471);
            this.panel1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageAdd);
            this.tabControl1.Controls.Add(this.tabPageEditar);
            this.tabControl1.Controls.Add(this.tabPageExcluir);
            this.tabControl1.Controls.Add(this.tabPagePesq);
            this.tabControl1.Location = new System.Drawing.Point(3, 4);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(769, 402);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPageAdd
            // 
            this.tabPageAdd.Controls.Add(this.cboCidade);
            this.tabPageAdd.Controls.Add(this.cboUfAdd);
            this.tabPageAdd.Controls.Add(this.label11);
            this.tabPageAdd.Controls.Add(this.txtMAeAdd);
            this.tabPageAdd.Controls.Add(this.label7);
            this.tabPageAdd.Controls.Add(this.label8);
            this.tabPageAdd.Controls.Add(this.txtTeladd);
            this.tabPageAdd.Controls.Add(this.label9);
            this.tabPageAdd.Controls.Add(this.txtIdadeAdd);
            this.tabPageAdd.Controls.Add(this.label10);
            this.tabPageAdd.Controls.Add(this.txtPaiAdd);
            this.tabPageAdd.Controls.Add(this.label6);
            this.tabPageAdd.Controls.Add(this.txtEmailAdd);
            this.tabPageAdd.Controls.Add(this.label5);
            this.tabPageAdd.Controls.Add(this.txtEndAdd);
            this.tabPageAdd.Controls.Add(this.label4);
            this.tabPageAdd.Controls.Add(this.txtNomeAdd);
            this.tabPageAdd.Controls.Add(this.label3);
            this.tabPageAdd.Controls.Add(this.dgAdcionar);
            this.tabPageAdd.Location = new System.Drawing.Point(4, 25);
            this.tabPageAdd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPageAdd.Name = "tabPageAdd";
            this.tabPageAdd.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPageAdd.Size = new System.Drawing.Size(761, 373);
            this.tabPageAdd.TabIndex = 0;
            this.tabPageAdd.Text = "Adcionar Alunos";
            this.tabPageAdd.UseVisualStyleBackColor = true;
            this.tabPageAdd.Enter += new System.EventHandler(this.tabPageAdd_Enter);
            // 
            // cboCidade
            // 
            this.cboCidade.FormattingEnabled = true;
            this.cboCidade.Location = new System.Drawing.Point(372, 112);
            this.cboCidade.Name = "cboCidade";
            this.cboCidade.Size = new System.Drawing.Size(87, 24);
            this.cboCidade.TabIndex = 19;
            // 
            // cboUfAdd
            // 
            this.cboUfAdd.FormattingEnabled = true;
            this.cboUfAdd.Location = new System.Drawing.Point(316, 112);
            this.cboUfAdd.Name = "cboUfAdd";
            this.cboUfAdd.Size = new System.Drawing.Size(50, 24);
            this.cboUfAdd.TabIndex = 18;
            this.cboUfAdd.SelectedIndexChanged += new System.EventHandler(this.cboUfAdd_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(316, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(23, 16);
            this.label11.TabIndex = 17;
            this.label11.Text = "UF";
            // 
            // txtMAeAdd
            // 
            this.txtMAeAdd.Location = new System.Drawing.Point(316, 158);
            this.txtMAeAdd.Name = "txtMAeAdd";
            this.txtMAeAdd.Size = new System.Drawing.Size(261, 22);
            this.txtMAeAdd.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(316, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 15;
            this.label7.Text = "Nome da Mãe";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(389, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "Cidade";
            // 
            // txtTeladd
            // 
            this.txtTeladd.Location = new System.Drawing.Point(316, 64);
            this.txtTeladd.Name = "txtTeladd";
            this.txtTeladd.Size = new System.Drawing.Size(143, 22);
            this.txtTeladd.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(316, 45);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "Telefone";
            // 
            // txtIdadeAdd
            // 
            this.txtIdadeAdd.Location = new System.Drawing.Point(316, 23);
            this.txtIdadeAdd.Name = "txtIdadeAdd";
            this.txtIdadeAdd.Size = new System.Drawing.Size(53, 22);
            this.txtIdadeAdd.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(316, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 16);
            this.label10.TabIndex = 9;
            this.label10.Text = "Idade";
            // 
            // txtPaiAdd
            // 
            this.txtPaiAdd.Location = new System.Drawing.Point(9, 158);
            this.txtPaiAdd.Name = "txtPaiAdd";
            this.txtPaiAdd.Size = new System.Drawing.Size(277, 22);
            this.txtPaiAdd.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "Nome do Pai";
            // 
            // txtEmailAdd
            // 
            this.txtEmailAdd.Location = new System.Drawing.Point(9, 112);
            this.txtEmailAdd.Name = "txtEmailAdd";
            this.txtEmailAdd.Size = new System.Drawing.Size(277, 22);
            this.txtEmailAdd.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Email";
            // 
            // txtEndAdd
            // 
            this.txtEndAdd.Location = new System.Drawing.Point(9, 64);
            this.txtEndAdd.Name = "txtEndAdd";
            this.txtEndAdd.Size = new System.Drawing.Size(277, 22);
            this.txtEndAdd.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Endereço";
            // 
            // txtNomeAdd
            // 
            this.txtNomeAdd.Location = new System.Drawing.Point(9, 23);
            this.txtNomeAdd.Name = "txtNomeAdd";
            this.txtNomeAdd.Size = new System.Drawing.Size(277, 22);
            this.txtNomeAdd.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Nome";
            // 
            // dgAdcionar
            // 
            this.dgAdcionar.AllowUserToAddRows = false;
            this.dgAdcionar.AllowUserToDeleteRows = false;
            this.dgAdcionar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgAdcionar.BackgroundColor = System.Drawing.Color.White;
            this.dgAdcionar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgAdcionar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAdcionar.GridColor = System.Drawing.Color.White;
            this.dgAdcionar.Location = new System.Drawing.Point(0, 186);
            this.dgAdcionar.Name = "dgAdcionar";
            this.dgAdcionar.ReadOnly = true;
            this.dgAdcionar.Size = new System.Drawing.Size(761, 169);
            this.dgAdcionar.TabIndex = 0;
            // 
            // tabPageEditar
            // 
            this.tabPageEditar.Controls.Add(this.txtCodigo);
            this.tabPageEditar.Controls.Add(this.label21);
            this.tabPageEditar.Controls.Add(this.cbCidade);
            this.tabPageEditar.Controls.Add(this.cbUF);
            this.tabPageEditar.Controls.Add(this.label12);
            this.tabPageEditar.Controls.Add(this.txtMae);
            this.tabPageEditar.Controls.Add(this.label13);
            this.tabPageEditar.Controls.Add(this.label14);
            this.tabPageEditar.Controls.Add(this.txtTelefone);
            this.tabPageEditar.Controls.Add(this.label15);
            this.tabPageEditar.Controls.Add(this.txtIdade);
            this.tabPageEditar.Controls.Add(this.label16);
            this.tabPageEditar.Controls.Add(this.txtPai);
            this.tabPageEditar.Controls.Add(this.label17);
            this.tabPageEditar.Controls.Add(this.txtEmail);
            this.tabPageEditar.Controls.Add(this.label18);
            this.tabPageEditar.Controls.Add(this.txtEndereco);
            this.tabPageEditar.Controls.Add(this.label19);
            this.tabPageEditar.Controls.Add(this.txtNome);
            this.tabPageEditar.Controls.Add(this.label20);
            this.tabPageEditar.Controls.Add(this.dtEditar);
            this.tabPageEditar.Location = new System.Drawing.Point(4, 25);
            this.tabPageEditar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPageEditar.Name = "tabPageEditar";
            this.tabPageEditar.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPageEditar.Size = new System.Drawing.Size(761, 373);
            this.tabPageEditar.TabIndex = 1;
            this.tabPageEditar.Text = "Editar Alunos";
            this.tabPageEditar.UseVisualStyleBackColor = true;
            this.tabPageEditar.Enter += new System.EventHandler(this.tabPageEditar_Enter);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(6, 33);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(47, 22);
            this.txtCodigo.TabIndex = 39;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 14);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 16);
            this.label21.TabIndex = 38;
            this.label21.Text = "Código";
            // 
            // cbCidade
            // 
            this.cbCidade.FormattingEnabled = true;
            this.cbCidade.Location = new System.Drawing.Point(373, 163);
            this.cbCidade.Name = "cbCidade";
            this.cbCidade.Size = new System.Drawing.Size(87, 24);
            this.cbCidade.TabIndex = 37;
            // 
            // cbUF
            // 
            this.cbUF.FormattingEnabled = true;
            this.cbUF.Location = new System.Drawing.Point(317, 163);
            this.cbUF.Name = "cbUF";
            this.cbUF.Size = new System.Drawing.Size(50, 24);
            this.cbUF.TabIndex = 36;
            this.cbUF.SelectedIndexChanged += new System.EventHandler(this.cbUF_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(317, 144);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 16);
            this.label12.TabIndex = 35;
            this.label12.Text = "UF";
            // 
            // txtMae
            // 
            this.txtMae.Location = new System.Drawing.Point(317, 209);
            this.txtMae.Name = "txtMae";
            this.txtMae.Size = new System.Drawing.Size(283, 22);
            this.txtMae.TabIndex = 34;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(317, 190);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 16);
            this.label13.TabIndex = 33;
            this.label13.Text = "Nome da Mãe";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(394, 144);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 16);
            this.label14.TabIndex = 32;
            this.label14.Text = "Cidade";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(317, 115);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(143, 22);
            this.txtTelefone.TabIndex = 31;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(317, 96);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 16);
            this.label15.TabIndex = 30;
            this.label15.Text = "Telefone";
            // 
            // txtIdade
            // 
            this.txtIdade.Location = new System.Drawing.Point(317, 74);
            this.txtIdade.Name = "txtIdade";
            this.txtIdade.Size = new System.Drawing.Size(53, 22);
            this.txtIdade.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(317, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 16);
            this.label16.TabIndex = 28;
            this.label16.Text = "Idade";
            // 
            // txtPai
            // 
            this.txtPai.Location = new System.Drawing.Point(6, 209);
            this.txtPai.Name = "txtPai";
            this.txtPai.Size = new System.Drawing.Size(284, 22);
            this.txtPai.TabIndex = 27;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 190);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(74, 16);
            this.label17.TabIndex = 26;
            this.label17.Text = "Nome do Pai";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(6, 163);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(284, 22);
            this.txtEmail.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 144);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 16);
            this.label18.TabIndex = 24;
            this.label18.Text = "Email";
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(6, 115);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(284, 22);
            this.txtEndereco.TabIndex = 23;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 96);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 16);
            this.label19.TabIndex = 22;
            this.label19.Text = "Endereço";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(6, 74);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(284, 22);
            this.txtNome.TabIndex = 21;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 16);
            this.label20.TabIndex = 20;
            this.label20.Text = "Nome";
            // 
            // dtEditar
            // 
            this.dtEditar.AllowUserToAddRows = false;
            this.dtEditar.AllowUserToDeleteRows = false;
            this.dtEditar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtEditar.BackgroundColor = System.Drawing.Color.White;
            this.dtEditar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtEditar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtEditar.GridColor = System.Drawing.Color.White;
            this.dtEditar.Location = new System.Drawing.Point(0, 237);
            this.dtEditar.Name = "dtEditar";
            this.dtEditar.ReadOnly = true;
            this.dtEditar.Size = new System.Drawing.Size(761, 129);
            this.dtEditar.TabIndex = 1;
            // 
            // tabPageExcluir
            // 
            this.tabPageExcluir.Controls.Add(this.groupBox3);
            this.tabPageExcluir.Controls.Add(this.dtExcluir);
            this.tabPageExcluir.Location = new System.Drawing.Point(4, 25);
            this.tabPageExcluir.Name = "tabPageExcluir";
            this.tabPageExcluir.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageExcluir.Size = new System.Drawing.Size(761, 373);
            this.tabPageExcluir.TabIndex = 2;
            this.tabPageExcluir.Text = "Excluir Alunos";
            this.tabPageExcluir.UseVisualStyleBackColor = true;
            this.tabPageExcluir.Enter += new System.EventHandler(this.tabPageExcluir_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtExcluir);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(292, 87);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Exluir Aluno";
            // 
            // txtExcluir
            // 
            this.txtExcluir.Location = new System.Drawing.Point(6, 52);
            this.txtExcluir.Name = "txtExcluir";
            this.txtExcluir.Size = new System.Drawing.Size(130, 22);
            this.txtExcluir.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Código do Aluno";
            // 
            // dtExcluir
            // 
            this.dtExcluir.AllowUserToAddRows = false;
            this.dtExcluir.AllowUserToDeleteRows = false;
            this.dtExcluir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtExcluir.BackgroundColor = System.Drawing.Color.White;
            this.dtExcluir.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtExcluir.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtExcluir.GridColor = System.Drawing.Color.White;
            this.dtExcluir.Location = new System.Drawing.Point(0, 96);
            this.dtExcluir.Name = "dtExcluir";
            this.dtExcluir.ReadOnly = true;
            this.dtExcluir.Size = new System.Drawing.Size(761, 281);
            this.dtExcluir.TabIndex = 1;
            // 
            // tabPagePesq
            // 
            this.tabPagePesq.Controls.Add(this.groupBox1);
            this.tabPagePesq.Controls.Add(this.dtPesquisar);
            this.tabPagePesq.Location = new System.Drawing.Point(4, 25);
            this.tabPagePesq.Name = "tabPagePesq";
            this.tabPagePesq.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePesq.Size = new System.Drawing.Size(761, 373);
            this.tabPagePesq.TabIndex = 3;
            this.tabPagePesq.Text = "Pesquisar Alunos";
            this.tabPagePesq.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.buttonPesquisar);
            this.groupBox1.Controls.Add(this.txtPesquisa);
            this.groupBox1.Location = new System.Drawing.Point(5, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(597, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pesquisa Aluno";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Digite o Valor para Pesquisa";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rbNome);
            this.groupBox2.Controls.Add(this.rbCodigo);
            this.groupBox2.Location = new System.Drawing.Point(455, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(136, 83);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo de Pesquisa";
            // 
            // rbNome
            // 
            this.rbNome.AutoSize = true;
            this.rbNome.Checked = true;
            this.rbNome.Location = new System.Drawing.Point(6, 21);
            this.rbNome.Name = "rbNome";
            this.rbNome.Size = new System.Drawing.Size(56, 20);
            this.rbNome.TabIndex = 4;
            this.rbNome.TabStop = true;
            this.rbNome.Text = "Nome";
            this.rbNome.UseVisualStyleBackColor = true;
            // 
            // rbCodigo
            // 
            this.rbCodigo.AutoSize = true;
            this.rbCodigo.Location = new System.Drawing.Point(6, 47);
            this.rbCodigo.Name = "rbCodigo";
            this.rbCodigo.Size = new System.Drawing.Size(65, 20);
            this.rbCodigo.TabIndex = 5;
            this.rbCodigo.TabStop = true;
            this.rbCodigo.Text = "Código";
            this.rbCodigo.UseVisualStyleBackColor = true;
            // 
            // buttonPesquisar
            // 
            this.buttonPesquisar.Location = new System.Drawing.Point(6, 71);
            this.buttonPesquisar.Name = "buttonPesquisar";
            this.buttonPesquisar.Size = new System.Drawing.Size(122, 23);
            this.buttonPesquisar.TabIndex = 2;
            this.buttonPesquisar.Text = "Pesquisar";
            this.buttonPesquisar.UseVisualStyleBackColor = true;
            this.buttonPesquisar.Click += new System.EventHandler(this.buttonPesquisar_Click);
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisa.Location = new System.Drawing.Point(134, 69);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(253, 25);
            this.txtPesquisa.TabIndex = 3;
            // 
            // dtPesquisar
            // 
            this.dtPesquisar.AllowUserToAddRows = false;
            this.dtPesquisar.AllowUserToDeleteRows = false;
            this.dtPesquisar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtPesquisar.BackgroundColor = System.Drawing.Color.White;
            this.dtPesquisar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dtPesquisar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtPesquisar.GridColor = System.Drawing.Color.White;
            this.dtPesquisar.Location = new System.Drawing.Point(0, 103);
            this.dtPesquisar.Name = "dtPesquisar";
            this.dtPesquisar.ReadOnly = true;
            this.dtPesquisar.Size = new System.Drawing.Size(761, 274);
            this.dtPesquisar.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.toolStrip1);
            this.panel2.Controls.Add(this.statusStrip1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 409);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 62);
            this.panel2.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCad_Aluno,
            this.btnAtualiza_Aluno,
            this.btnDeletar,
            this.toolStripSeparator1,
            this.btnSair});
            this.toolStrip1.Location = new System.Drawing.Point(0, 1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(776, 39);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblDateTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 40);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip1.Size = new System.Drawing.Size(776, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblDateTime
            // 
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(71, 17);
            this.lblDateTime.Text = "lblDateTime";
            // 
            // btnCad_Aluno
            // 
            this.btnCad_Aluno.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCad_Aluno.Image = global::CadastroDeAlunos.Properties.Resources.alunos;
            this.btnCad_Aluno.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCad_Aluno.Name = "btnCad_Aluno";
            this.btnCad_Aluno.Size = new System.Drawing.Size(36, 36);
            this.btnCad_Aluno.Click += new System.EventHandler(this.btnCad_Aluno_Click);
            // 
            // btnAtualiza_Aluno
            // 
            this.btnAtualiza_Aluno.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAtualiza_Aluno.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualiza_Aluno.Image")));
            this.btnAtualiza_Aluno.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAtualiza_Aluno.Name = "btnAtualiza_Aluno";
            this.btnAtualiza_Aluno.Size = new System.Drawing.Size(36, 36);
            this.btnAtualiza_Aluno.Click += new System.EventHandler(this.btnAtualiza_Aluno_Click);
            // 
            // btnDeletar
            // 
            this.btnDeletar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnDeletar.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletar.Image")));
            this.btnDeletar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDeletar.Name = "btnDeletar";
            this.btnDeletar.Size = new System.Drawing.Size(36, 36);
            this.btnDeletar.Click += new System.EventHandler(this.btnDeletar_Click);
            // 
            // btnSair
            // 
            this.btnSair.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnSair.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSair.Image = ((System.Drawing.Image)(resources.GetObject("btnSair.Image")));
            this.btnSair.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(36, 36);
            this.btnSair.Text = "Sair";
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // FrmCrud
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 471);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FrmCrud";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema de Cadastro de Alunos";
            this.Load += new System.EventHandler(this.FrmCrud_Load);
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageAdd.ResumeLayout(false);
            this.tabPageAdd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAdcionar)).EndInit();
            this.tabPageEditar.ResumeLayout(false);
            this.tabPageEditar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtEditar)).EndInit();
            this.tabPageExcluir.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtExcluir)).EndInit();
            this.tabPagePesq.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtPesquisar)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblDateTime;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageAdd;
        private System.Windows.Forms.TabPage tabPageEditar;
        private System.Windows.Forms.TabPage tabPageExcluir;
        private System.Windows.Forms.TabPage tabPagePesq;
        private System.Windows.Forms.ToolStripButton btnCad_Aluno;
        private System.Windows.Forms.ToolStripButton btnAtualiza_Aluno;
        private System.Windows.Forms.ToolStripButton btnDeletar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnSair;
        private System.Windows.Forms.DataGridView dgAdcionar;
        private System.Windows.Forms.DataGridView dtEditar;
        private System.Windows.Forms.DataGridView dtExcluir;
        private System.Windows.Forms.DataGridView dtPesquisar;
        private System.Windows.Forms.RadioButton rbCodigo;
        private System.Windows.Forms.RadioButton rbNome;
        private System.Windows.Forms.TextBox txtPesquisa;
        private System.Windows.Forms.Button buttonPesquisar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtExcluir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMAeAdd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTeladd;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtIdadeAdd;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPaiAdd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtEmailAdd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtEndAdd;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNomeAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboCidade;
        private System.Windows.Forms.ComboBox cboUfAdd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cbCidade;
        private System.Windows.Forms.ComboBox cbUF;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMae;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTelefone;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtIdade;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPai;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label21;
    }
}

