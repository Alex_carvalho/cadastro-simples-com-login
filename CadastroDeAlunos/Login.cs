﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadastroDeAlunos.Infra;

namespace CadastroDeAlunos
{
    public partial class Login : Form
    {
        private SqlConnection sqlConn = null;
        private string strCoon = @"Data Source=ALEXANDRE-VAIO\alex;Initial Catalog=CadastroAlunos;Persist Security Info=True;User ID=sa;Password=12345";
        private string _Sql = string.Empty;

        public bool Logado = false;
        
        public Login()
        {
            InitializeComponent();
        }

        public void Logar()
        {
            sqlConn = new SqlConnection(strCoon);
            string usu, pwd;

            try
            {
                usu = txtUsuario.Text;
                pwd = txtSenha.Text;

                _Sql = "SELECT COUNT(IdUsuario) FROM USUARIO WHERE Usuario = @USUARIO AND Senha = @SENHA"; 

                var cmd = new SqlCommand(_Sql,sqlConn);

                cmd.Parameters.Add("@USUARIO", SqlDbType.VarChar).Value = usu;
                cmd.Parameters.Add("@SENHA", SqlDbType.VarChar).Value = pwd;

                sqlConn.Open();
                
                int v = (int)cmd.ExecuteScalar();

                if (v > 0)
                {
                    MessageBox.Show("Usuário logado com sucesso.");
                    Logado = true;
                    this.Dispose();
                }
                else
                {
                    MessageBox.Show("Erro ao logar");
                    Logado = false;
                }
            }
            catch (SqlException erro)
            {
                MessageBox.Show(erro + "Erro no Banco");
            }
        }

        private void BtnCadastrar_Click(object sender, EventArgs e)
        {
            Logar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
