﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CadastroDeAlunos.Classes;
using CadastroDeAlunos.Infra;

namespace CadastroDeAlunos
{
    public partial class FrmCrud : Form
    {
        public FrmCrud()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime datahora = DateTime.Now;
            lblDateTime.Text = string.Format("Data: {0} Hora: {1}", datahora.ToLongDateString(), datahora.ToLongTimeString());
        }

        private void FrmCrud_Load(object sender, EventArgs e)
        {
            timer1_Tick(e, e);
        }

        private void btnCad_Aluno_Click(object sender, EventArgs e)
        {
            var obj = new SisDBA();
            var aluno = new Aluno();

            try
            {
                PreencheAluno(aluno);

                if (obj.Insert(aluno))
                    MessageBox.Show("Inserido com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show("Erro ao Inserir Aluno!", "Erro.", MessageBoxButtons.OK, MessageBoxIcon.Error);

                dgAdcionar.DataSource = obj.ListaGrid();
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro + "Erro Ocorrido.");
            }
        }

        public void PreencheAluno(Aluno aluno)
        {
            aluno.nome = txtNomeAdd.Text;
            aluno.Idade = Convert.ToInt32(txtIdadeAdd.Text);
            aluno.Endereco = txtEndAdd.Text;
            aluno.Telefone = txtTeladd.Text;
            aluno.Email = txtEmailAdd.Text;
            aluno.Cidade = cboCidade.Text;
            aluno.Uf = cboUfAdd.Text;
            aluno.NomePAi = txtPaiAdd.Text;
            aluno.NomeMae = txtMAeAdd.Text;
        }

        private void btnAtualiza_Aluno_Click(object sender, EventArgs e)
        {
            var obj = new SisDBA();
            var aluno = new Aluno();

            try
            {
                AtualizarAluno(aluno);

                if (obj.UpDate(aluno))
                {
                    MessageBox.Show("Atualizado com sucesso", "Sucesso", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    tabPageEditar_Enter(e,e);
                }
                else
                {
                    MessageBox.Show("Erro ao Atualizar Aluno!", "Erro.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception erro)
            {
                MessageBox.Show(erro + "Erro Ocorrido.");
            }
        }

        public void AtualizarAluno(Aluno aluno)
        {
            aluno.Codigo = Convert.ToInt32(txtCodigo.Text);
            aluno.nome = txtNome.Text;
            aluno.Idade = Convert.ToInt32(txtIdade.Text);
            aluno.Endereco = txtEndereco.Text;
            aluno.Telefone = txtTelefone.Text;
            aluno.Email = txtEmail.Text;
            aluno.Cidade = cbCidade.Text;
            aluno.Uf = cbUF.Text;
            aluno.NomePAi = txtPai.Text;
            aluno.NomeMae = txtMae.Text;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Você tem certeza que deseja sair do sistema?", "Mensagem do Sistema",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            var obj = new SisDBA();

            int codAluno = int.Parse(txtExcluir.Text);

            if (obj.Delete(codAluno))
            {
                MessageBox.Show("Deletado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tabPageExcluir_Enter(e, e);
            }
            else
            {
                MessageBox.Show("Erro ao Deletar Aluno!", "Erro.", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonPesquisar_Click(object sender, EventArgs e)
        {
            var obj = new SisDBA();

            string sql;

            if (rbNome.Checked)
            {
                sql = "Select [Nome],[Idade],[Endereco],[Telefone],[Email],[Cidade] From Alunos Where Nome Like @valor";
                dtPesquisar.DataSource = obj.Pesquisar(sql, "%" + txtPesquisa.Text + "%");
            }
            else
            {
                sql = "Select [Nome],[Idade],[Endereco],[Telefone],[Email],[Cidade] From Alunos Where Id_Aluno = @valor";
                dtPesquisar.DataSource = obj.Pesquisar(sql, txtPesquisa.Text);
            }
        }

        private void tabPageExcluir_Enter(object sender, EventArgs e)
        {
            var obj = new SisDBA();

            dtExcluir.DataSource = obj.ListaGrid();
        }

        private void tabPageEditar_Enter(object sender, EventArgs e)
        {
            var obj = new SisDBA();
            cbUF.DataSource = obj.ListaUf();
            cbUF.DisplayMember = "UF";

            dtEditar.DataSource = obj.ListaGrid();
        }

        private void tabPageAdd_Enter(object sender, EventArgs e)
        {
            var obj = new SisDBA();
            cboUfAdd.DataSource = obj.ListaUf();
            cboUfAdd.DisplayMember = "UF";

            dgAdcionar.DataSource = obj.ListaGrid();
        }

        private void cboUfAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
            var obj = new SisDBA();
            cboCidade.DataSource = obj.ListaCidade(cboUfAdd.Text);
            cboCidade.DisplayMember = "NOME"; 
        }

        private void cbUF_SelectedIndexChanged(object sender, EventArgs e)
        {
            var obj = new SisDBA();
            cbCidade.DataSource = obj.ListaCidade(cbUF.Text);
            cbCidade.DisplayMember = "NOME";
        }        
    }
}
